<?php
require_once("../../../vendor/autoload.php");



use App\BITM\SEIP128778\SummaryOfOrganization\SummaryOfOrganization;

$obj= new SummaryOfOrganization();


$all_organization= $obj->index();

?>
<!--table-->

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title> </title>

    <!-- Latest compiled and minified CSS -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/bootstrap-theme.min.css">
    <!-- <link rel="stylesheet" href="../../../resource/Bootstrap/css/booktitle.css"> -->
</head>

<style>
    .main{
        margin-top: 10%;
        margin-left: 15%;
        margin-right:15%;
        background-color: #679a9f;



    }
    body{
        background-image:url("../../../resource/assets/images/general1.jpg");

        background-repeat:no-repeat;
        background-size: 100% 925px;

    }



</style>

<body  >
<div class="container ">

    <div class="main">



        <div class="panel panel-default" >
            <div class="panel-heading">
                <div class="panel-heading">
                    <h1 style="text-align: center"> Organization List</h1>


                </div>
            </div>





            <div class="panel-body">
                <div class="table-responsive" >
                    </br></br></br></br>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>ID</th>
                            <th>Organization Name</th>
                            <th>Organization Summary</th>

                            <th>Action</th>

                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <?php
                            $serial=0;

                            foreach($all_organization as $result){
                            $serial++; ?>
                            <td><?php echo $serial?></td>
                            <td><?php echo $result['id']?></td>
                            <td><?php echo $result['organization_name']?></td>
                            <td><?php echo $result['organization_summary']?></td>
                            <td><a href="view.php?id=<?php echo $result['id']  ?>" class="btn btn-primary" role="button">View</a>
                                <a href="edit.php?id=<?php echo $result['id'] ?>"  class="btn btn-info" role="button">Edit</a>
                                <a href="delete.php?id=<?php echo $result['id'] ?>" class="btn btn-danger" role="button" id="delete"  Onclick="return ConfirmDelete()">Delete</a>
                                <a href="trash.php?id=<?php echo $result['id'] ?>"  class="btn btn-info" role="button">Trash</a>
                            </td>

                        </tr>
                        <?php }?>


                        </tbody>
                    </table>

                </div>

            </div>

        </div>

    </div>
</div>



<script src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

</body>
</html>



