<?php
require_once("../../../vendor/autoload.php");

use App\BITM\SEIP128778\Hobbies\Hobbies;
use App\BITM\SEIP128778\Utility\Utility;

$obj= new Hobbies();
$id=$_GET['id'];



$selected_person= $obj->view($id);
//Utility::d($selected_person);


$hobbies_array=explode(",",$selected_person['hobbies']);
//Utility::dd($hobbies_array);


?>

    <!DOCTYPE html>
    <html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Hobbies</title>
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/hobbies.css" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
</head>
<body>


<div class="container">


    <div class="row vertical-offset-100">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-heading">
                        <h1>Update Hobbies</h1>

                    </div>




                </div>
                <div class="panel-body">
                    <form accept-charset="UTF-8" role="form" class="form-booktitle" method="Post" action="update.php">
                        <fieldset>
                            <label class="panel-input">
                                <div class="input_result"></div>
                            </label>
                            <input class="form-control" value="<?php echo $selected_person['name'] ?>"  name="name" type="text">


                            Select your hobbies:<br />
                            <input  type="checkbox" name="hobbies[]" value="swimming" <?php if(in_array("swimming",$hobbies_array)) : ?> checked <?php endif; ?> />swimming<br />
                            <input  type="checkbox" name="hobbies[]" value="reading" <?php if(in_array("reading",$hobbies_array)) : ?> checked <?php endif; ?> />reading<br />
                            <input type="checkbox" name="hobbies[]" value="gaming" <?php if(in_array("gaming",$hobbies_array)) : ?> checked <?php endif; ?>/>gaming<br />
                            <input  type="checkbox" name="hobbies[]" value="running" <?php if(in_array("running",$hobbies_array)) : ?> checked <?php endif; ?>/>running<br />
                            <input  type="checkbox" name="hobbies[]" value="dancing" <?php if(in_array("dancing",$hobbies_array)) : ?> checked <?php endif; ?>/>dancing
                            </br>
                            <input type="hidden" name="id" value="<?php echo $id?>" >
                            <input class="btn btn-lg btn-success btn-block" type="submit" name="submit" value="Update »">
                        </fieldset>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>





</body>
</html>