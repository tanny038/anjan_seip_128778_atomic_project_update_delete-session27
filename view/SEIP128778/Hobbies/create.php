<?php
require_once("../../../vendor/autoload.php");
use App\BITM\SEIP128778\Message\Message;

if(!isset( $_SESSION)) session_start();
$message=Message::message();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Hobbies</title>
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/bootstrap-theme.min.css">
  <link rel="stylesheet" href="../../../resource/Bootstrap/css/hobbies.css" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
</head>
<body>


<div class="container">

    <div class="row vertical-offset-100">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-heading">
                        <h1>Add Hobbies</h1>

                    </div>




                </div>
                <div class="panel-body">
                    <form accept-charset="UTF-8" role="form" class="form-hobbies" method="Post" action="store.php">
                        <fieldset>
                            <label class="panel-input">
                                <div class="input_result"></div>
                            </label>
                            <input class="form-control" placeholder="enter  Name here" name="name" type="text">


                                Select your hobbies:<br />
                                <input  type="checkbox" name="hobbies[]" value="swimming" />swimming<br />
                                <input  type="checkbox" name="hobbies[]" value="reading" />reading<br />
                                <input type="checkbox" name="hobbies[]" value="gaming" />gaming<br />
                                <input  type="checkbox" name="hobbies[]" value="running" />running<br />
                                <input  type="checkbox" name="hobbies[]" value="dancing" />dancing
                            </br>
                            <input class="btn btn-lg btn-success btn-block" type="submit" name="submit" value="Add »">
                        </fieldset>
                    </form>
                    <div id="confirmation_message">
                        <?php echo $message;?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<script>
    $(document).ready(function(){
        $(function() {
            $('#confirmation_message').delay(5000).fadeOut();
        });

    });
</script>

</body>
</html>