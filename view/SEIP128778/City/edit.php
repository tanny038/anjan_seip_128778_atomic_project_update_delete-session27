<?php
require_once("../../../vendor/autoload.php");

use App\BITM\SEIP128778\City\City;


$obj= new City();
$id=$_GET['id'];



$selected_person= $obj->view($id);

?>

    <!DOCTYPE html>
    <html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/city.css" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
</head>
<body>


<div class="container">


    <div class="row vertical-offset-100">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-heading">
                        <h1>Update City</h1>

                        <div class="row-fluid user-row">
                            <img src="../../../resource/assets/images/book%20icon.png" class="img-responsive icon" alt="Conxole Admin"/>
                        </div>
                    </div>




                </div>
                <div class="panel-body">
                    <form accept-charset="UTF-8" role="form" class="form-booktitle" method="Post" action="update.php">
                        <fieldset>
                            <label class="panel-input">
                                <div class="input_result"></div>
                            </label>
                            <input class="form-control" value="<?php echo $selected_person['name'] ?>"  name="name" type="text">

                            <select name="city_name" class="form-control">
                                <option <?php if($selected_person['city_name']=="Chittagong"):?>selected<?php endif ?>>Chittagong</option>
                                <option <?php if($selected_person['city_name']=="dhaka"):?>selected<?php endif ?>>dhaka</option>
                                <option <?php if($selected_person['city_name']=="Comilla"):?>selected<?php endif ?>>Comilla</option>
                                <option <?php if($selected_person['city_name']=="khulna"):?>selected<?php endif ?>>khulna</option>
                                <option <?php if($selected_person['city_name']=="rajshahi"):?>selected<?php endif ?>>rajshahi</option>
                            </select>

                            </br>
                            <input type="hidden" name="id" value="<?php echo $id?>" >
                            <input class="btn btn-lg btn-success btn-block" type="submit" name="submit" value="Update »">
                        </fieldset>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>





</body>
</html>