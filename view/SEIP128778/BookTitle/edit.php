<?php
require_once("../../../vendor/autoload.php");
use App\BITM\SEIP128778\Message\Message;

if(!isset( $_SESSION)) session_start();
$message=Message::message();
$objBookTitle= new \App\BITM\SEIP128778\BookTitle\BookTitle();
$id=$_GET['id'];



$selected_book= $objBookTitle->view($id);

?>

    <!DOCTYPE html>
    <html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/booktitle.css" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
</head>
<body>


<div class="container">


    <div class="row vertical-offset-100">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-heading">
                        <h1>Update BookTitle</h1>

                        <div class="row-fluid user-row">
                            <img src="../../../resource/assets/images/book%20icon.png" class="img-responsive icon" alt="Conxole Admin"/>
                        </div>
                    </div>




                </div>
                <div class="panel-body">
                    <form accept-charset="UTF-8" role="form" class="form-booktitle" method="Post" action="update.php">
                        <fieldset>
                            <label class="panel-input">
                                <div class="input_result"></div>
                            </label>
                            <input class="form-control" value="<?php echo $selected_book['book_title'] ?>"  name="book_title" type="text">
                            <input class="form-control" value="<?php echo $selected_book['author_name'] ?>" name="author_name" type="text">
                            </br>
                            <input type="hidden" name="id" value="<?php echo $id?>" >
                            <input class="btn btn-lg btn-success btn-block" type="submit" name="submit" value="Update »">
                        </fieldset>
                    </form>
                    <div id="confirmation_message">
                        <?php echo $message;?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<script>
    $(document).ready(function(){
        $(function() {
            $('#confirmation_message').delay(5000).fadeOut();
        });

    });
</script>

</body>
    </html><?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 11/13/2016
 * Time: 5:49 PM
 */