<?php
namespace App\BITM\SEIP128778\Email;
use App\BITM\SEIP128778\Message\Message;
use App\BITM\SEIP128778\Utility\Utility;
use App\BITM\SEIP128778\Model\Database as DB;
use PDO;

class Email extends DB{
    public $id="";
    public $name="";
    public $email="";



    public function __construct(){
        parent::__construct();
    }


        public function index($fetchMode='ASSOC'){
            $fetchMode = strtoupper($fetchMode);
            $DBH=$this->connection;
            $sth=$DBH->prepare("select * from email");
            $sth->execute();
            if(substr_count($fetchMode,'OBJ') > 0)
                $sth->setFetchMode(PDO::FETCH_OBJ);
            else
                $sth->setFetchMode(PDO::FETCH_ASSOC);

            $all_email=$sth->fetchAll();


            return  $all_email;
        }




    public function view($id){
        $DBH=$this->connection;
        $sth=$DBH->prepare("select * from email Where id=$id");
        $sth->execute();
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $selected_email=$sth->fetch();

        return  $selected_email;

    }




    public function setData($data=null){
        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }
        if(array_key_exists('name',$data)){
            $this->name=$data['name'];

        }
        if(array_key_exists('email',$data)){
            $this->email=$data['email'];

        }
    }
    public function store(){
       $dbh=$this->connection;
        $values=array($this->name,$this->email);
        $query="insert into email(name,email) VALUES (?,?)";
        $sth=$dbh->prepare($query);
        $sth->execute($values);
        if($sth)
        Message::message("<div id='msg'></div><h3 align='center'>[ Name: $this->name ] ,
 [ email: $this->email ] <br> Data Has Been Inserted Successfully!</h3></div>");

        else        Message::message("<div id='msg'></div><h3 align='center'>[ Name: $this->name ] ,
 [ email: $this->email ] <br> Data Hasn't Been Inserted Successfully!</h3></div>");

        Utility::redirect('create.php');



    }

    public function update(){
        $dbh=$this->connection;
        $values=array($this->name,$this->email);

        //var_dump($values);


        $query='UPDATE email  SET name  = ?   , email = ? where id ='.$this->id;

        $sth=$dbh->prepare($query);
        $sth->execute($values);
        if($sth)
        { Message::message("<div id='msg'><h3 align='center'>[ BookTitle: $this->name ] ,
               [ AuthorName: $this->email ] <br> Data Has Been Inserted Successfully!</h3></div>");
        }

        else
            Message::message("<div id='msg'></div><h3 align='center'> <br> Data Hasn't Been Inserted !</h3></div>");
        Utility::redirect('index.php');



    }
    public function delete($id){
        $DBH=$this->connection;
        $sth=$DBH->prepare("delete  from email Where id=$id");
        $sth->execute();
        if($sth)
        { Message::message("<div id='msg'><h3 align='center'>
               <br> Data Has deleted Successfully!</h3></div>");
        }

        else
            Message::message("<div id='msg'></div><h3 align='center'> <br> Data Hasn't Been Inserted !</h3></div>");
        Utility::redirect('index.php');


    }


}

