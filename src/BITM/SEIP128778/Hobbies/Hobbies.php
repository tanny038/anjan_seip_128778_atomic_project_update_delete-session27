<?php
namespace App\BITM\SEIP128778\Hobbies;
use App\BITM\SEIP128778\Message\Message;
use App\BITM\SEIP128778\Utility\Utility;
use App\BITM\SEIP128778\Model\Database as DB;
use PDO;

class Hobbies extends DB{
    public $id="";
    public $name="";
    public $hobbies="";



    public function __construct(){
        parent::__construct();
    }


        public function index($fetchMode='ASSOC'){
            $fetchMode = strtoupper($fetchMode);
            $DBH=$this->connection;
            $sth=$DBH->prepare("select * from hobbies");
            $sth->execute();
            if(substr_count($fetchMode,'OBJ') > 0)
                $sth->setFetchMode(PDO::FETCH_OBJ);
            else
                $sth->setFetchMode(PDO::FETCH_ASSOC);

            $all_hobbies=$sth->fetchAll();


            return  $all_hobbies;
        }









public function view($id){
        $DBH=$this->connection;
        $sth=$DBH->prepare("select * from hobbies Where id=$id");
        $sth->execute();
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $selected_person=$sth->fetch();

        return  $selected_person;

    }


    public function update(){
        $dbh=$this->connection;
        $values=array($this->name,$this->hobbies);

        //var_dump($values);


        $query='UPDATE hobbies  SET  name = ?   , hobbies= ? where id ='.$this->id;



        //    $query='UPDATE book_title  SET book_title  = ?   , author_name = ? where id ='.$this->id;

        $sth=$dbh->prepare($query);
        $sth->execute($values);
        if($sth)
        { Message::message("<div id='msg'><h3 align='center'>[ name: $this->name ] ,
               [ Hobbies: $this->birthday ] <br> Data Has Been Updated Successfully!</h3></div>");
        }

        else
            Message::message("<div id='msg'></div><h3 align='center'> <br> Data Hasn't Been Updated !</h3></div>");
        Utility::redirect('index.php');



    }
    public function delete($id){
        $DBH=$this->connection;
        $sth=$DBH->prepare("delete  from hobbies Where id=$id");
        $sth->execute();
        if($sth)
        { Message::message("<div id='msg'><h3 align='center'>
               <br> Data Has deleted Successfully!</h3></div>");
        }

        else
            Message::message("<div id='msg'></div><h3 align='center'> <br> Data Hasn't Been Inserted !</h3></div>");
        Utility::redirect('index.php');


    }


    public function setData($data=null){
        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }
        if(array_key_exists('name',$data)){
            $this->name=$data['name'];

        }
        if(array_key_exists('hobbies',$data)){
            $this->hobbies=$data['hobbies'];

        }
    }
    public function store(){
       $dbh=$this->connection;
        $values=array($this->name,$this->hobbies);
        $query="insert into hobbies(name,hobbies) VALUES (?,?)";
        $sth=$dbh->prepare($query);
        $sth->execute($values);
        if($sth)
        Message::message("<div id='msg'></div><h3 align='center'>[ Name: $this->name ] , [Hobbies: $this->hobbies ] <br> Data Has Been Inserted Successfully!</h3></div>");
else
        Message::message("<div id='msg'></div><h3 align='center'>[ Name: $this->name ] , [Hobbies: $this->hobbies ] <br> Data Hasn't Been Inserted Successfully!</h3></div>");

        Utility::redirect('create.php');



    }


}

